<?php

class Dispatcher
{
    /**
     * @var array
     */
    private $versions = array('https://www.evalnova.com/survey/',
                         'https://www.evalnova.com/survey_v2/',
                         'https://www.evalnova.com/v3/app/'
                        );
    /**
     * @var \Doctrine\DBAL\Connection|null
     */
    private $conn = null;

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct()
    {
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = array(
            'dbname' => DATABASE,
            'user' => USERNAME,
            'password' => PASSWORD,
            'host' => HOSTNAME,
            'driver' => 'pdo_mysql',
        );
        $this->conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
    }

    /**
     * Dispatch function
     *
     * @param string $device_id
     *
     */

    public function dispatch($device_id)
    {
        $queryBuilder = $this->conn->createQueryBuilder();

        if (isset($device_id)) {
            $queryBuilder
                ->select('versionpath', 'version')
                ->from('dispositivos')
                ->where('md5id like ?')
                ->setMaxResults(1)
                ->setParameter(0, $device_id);

			//$queryBuilder->prepare();
			
            $device_query =  $queryBuilder->execute();

            $device = isset($device_query)?$device_query->fetch():null;

			$this->redirect($this->versions[$device['version']-1].'?id='.$device_id);
			
			//$this->redirect('https://www.evalnova.com/v3/app/?id='.$device_id);
			
			/*
            if(isset($device['versionpath'])){
                $this->redirect($device['versionpath'].'?id='.$device_id);
            }
            elseif(isset($device['version'])){

				//$this->redirect($this->versions[$device['version']-1].'?id='.$device_id);
				
                switch ($device['version'])
                {
                    case 1:
                        $this->redirect($this->versions[0].'?id='.$device_id);
                        break;
                    case 2:
                        $this->redirect($this->versions[1].'?id='.$device_id);
                        break;
                    case 3:
                        $this->redirect($this->versions[2].'?id='.$device_id);
                        break;
                }
				
            }
		*/
        }
    }

    /**
     * redirect function
     *
     * @param $url
     * @param bool $permanent
     */
    private function redirect($url, $permanent = true)
    {
        header('Location: ' . $url, true, $permanent ? 301 : 302);
		//header('Location: https://www.evalnova.com/dispatcher/?id=d0b63496b650' , false);
        exit();
    }
}
